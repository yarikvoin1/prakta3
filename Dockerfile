FROM python:alpine

LABEL version="1.0.3"

WORKDIR /testdir/python

COPY . /testdir/python/

CMD [ "python", "calc.py" ]